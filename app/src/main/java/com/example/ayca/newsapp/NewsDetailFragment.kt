package com.example.ayca.newsapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.squareup.picasso.Picasso

class NewsDetailFragment : Fragment(), View.OnClickListener {
    lateinit var newsImage: ImageView
    lateinit var titleTv: TextView
    lateinit var descriptionTv: TextView
    lateinit var authorTv: TextView
    lateinit var urlTv: TextView
    lateinit var publishedAtTv: TextView
    lateinit var backBtn: Button
    lateinit var companyNameTV: TextView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.news_detail_fragment, container, false)
        initView(view)
        getBundleData()
        return view
    }

    private fun getBundleData() {

        authorTv.text = this.arguments?.getString("author")
        urlTv.text = this.arguments?.getString("url")
        descriptionTv.text = this.arguments?.getString("description")
        publishedAtTv.text = this.arguments?.getString("publishedAt")
        titleTv.text = this.arguments?.getString("title")
        companyNameTV.text = this.arguments?.getString("source")

        if (this.arguments?.getString("urlToImage").isNullOrEmpty()) {
            newsImage.visibility = View.GONE
        } else {
            picassoImage(newsImage, this.arguments?.getString("urlToImage")!!)
        }

    }

    private fun initView(view: View) {
        newsImage = view.findViewById(R.id.news_image)
        titleTv = view.findViewById(R.id.title_tv)
        descriptionTv = view.findViewById(R.id.description_tv)
        authorTv = view.findViewById(R.id.author_tv)
        urlTv = view.findViewById(R.id.url_tv)
        publishedAtTv = view.findViewById(R.id.publish_date_tv)
        backBtn = view.findViewById(R.id.back_btn)
        companyNameTV = view.findViewById(R.id.company_name_tv)

        backBtn.setOnClickListener(this)
    }

    private fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(350, 350).into(imageView)
    }

    override fun onClick(v: View?) {
        Navigation.findNavController(v!!).popBackStack()
    }
}