package com.example.ayca.newsapp.model

import com.google.gson.annotations.SerializedName

data class MainData(
    @SerializedName("articles")
    val articles: List<Articles>
)