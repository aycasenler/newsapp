package com.example.ayca.newsapp.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.newsapp.R
import com.example.ayca.newsapp.model.Articles
import com.squareup.picasso.Picasso

class SportNewsAdapter(private var articleList: List<Articles>, private var context: Context) :
    RecyclerView.Adapter<SportNewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(R.layout.sport_news_item_design, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return articleList.size
    }

    override fun onBindViewHolder(holder: SportNewsAdapter.ViewHolder, position: Int) {

        val articles = articleList.get(position)
        holder.descriptionTv.text = articles.description
        holder.titleTv.text = articles.title
        holder.publishedDateTv.text = articles.publishedAt
        holder.companyNameTv.text= articles.source.name
        if (articles.urlToImage.isNullOrEmpty()) {
            holder.image.visibility = View.GONE
        } else {
            picassoImage(holder.image, articles.urlToImage)
        }
        holder.itemView.setOnClickListener {
            clickNews(position, it)
        }
    }

    private fun clickNews(position: Int, view: View) {
        val bundle = Bundle()
        bundle.putString("author", articleList[position].author)
        bundle.putString("urlToImage", articleList[position].urlToImage)
        bundle.putString("url", articleList[position].url)
        bundle.putString("description", articleList[position].description)
        bundle.putString("publishedAt", articleList[position].publishedAt)
        bundle.putString("title", articleList[position].title)
        bundle.putString("source", articleList[position].source.name)

        Navigation.findNavController(view).navigate(R.id.newsDetailFragment, bundle)
    }


    inner class ViewHolder(itemLayoutView: View) : RecyclerView.ViewHolder(itemLayoutView) {
        var titleTv: TextView = itemLayoutView.findViewById(R.id.title_tv)
        var descriptionTv: TextView = itemLayoutView.findViewById(R.id.description_tv)
        var image: ImageView = itemLayoutView.findViewById(R.id.image)
        var publishedDateTv : TextView = itemLayoutView.findViewById(R.id.publish_date_tv)
        var companyNameTv : TextView = itemLayoutView.findViewById(R.id.company_name_tv)

    }

    fun picassoImage(imageView: ImageView, url: String) {
        Picasso.get().load(url).resize(350,350).into(imageView)
    }
}

