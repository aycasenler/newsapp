package com.example.ayca.newsapp.model

import com.google.gson.annotations.SerializedName

data class Source(
    @SerializedName("name")
    val name: String
)