package com.example.ayca.newsapp

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ayca.newsapp.adapter.SportNewsAdapter
import com.example.ayca.newsapp.model.Articles
import com.example.ayca.newsapp.model.MainData
import com.example.ayca.newsapp.retrofit.ApiClient
import net.cachapa.expandablelayout.ExpandableLayout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class HomeScreenFragment : Fragment(), View.OnClickListener {
    lateinit var recyclerViewSportNews: RecyclerView
    lateinit var selectDateLL: LinearLayout
    lateinit var loadingAnimationLL: LinearLayout
    lateinit var dateExpandable: ExpandableLayout
    lateinit var articleList: List<Articles>
    lateinit var adapter: SportNewsAdapter
    lateinit var selectFromDateBtn: Button
    lateinit var selectToDateBtn: Button
    lateinit var sortByPopularityBtn: Button
    lateinit var sortByPublishedAtBtn: Button
    lateinit var topSelectedSeparator: View
    lateinit var latestSelectedSeparator: View
    private var datePickerDialog: DatePickerDialog? = null
    private var calendar: Calendar? = null
    private var year: Int = 0
    private var month: Int = 0
    private var dayOfMonth: Int = 0
    lateinit var fromDate: String
    lateinit var toDate: String
    private var topClicked: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.home_screen_fragment, container, false)
        initView(view)

        return view
    }

    private fun initView(view: View) {
        selectDateLL = view.findViewById(R.id.select_date_ll)
        loadingAnimationLL = view.findViewById(R.id.loading_animation_ll)
        selectFromDateBtn = view.findViewById(R.id.select_from_date_btn)
        selectToDateBtn = view.findViewById(R.id.select_to_date_btn)
        dateExpandable = view.findViewById(R.id.date_expandable_view)
        topSelectedSeparator = view.findViewById(R.id.top_selected_separator)
        latestSelectedSeparator = view.findViewById(R.id.latest_selected_separator)
        sortByPopularityBtn = view.findViewById(R.id.sort_by_popularity_btn)
        sortByPublishedAtBtn = view.findViewById(R.id.sort_by_published_at_btn)
        articleList = mutableListOf()
        recyclerViewSportNews = view.findViewById(R.id.sport_news_recycler_view)
        recyclerViewSportNews.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        currentTime()
        getSportNews(fromDate, toDate, "popularity")

        selectFromDateBtn.setOnClickListener(this)
        selectToDateBtn.setOnClickListener(this)
        sortByPublishedAtBtn.setOnClickListener(this)
        sortByPopularityBtn.setOnClickListener(this)
        selectDateLL.setOnClickListener(this)
    }


    @SuppressLint("SimpleDateFormat")
    private fun currentTime() {
        val date = Date()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val formatted: String = formatter.format(date)
        toDate = formatted
        fromDate = formatter.format(getDaysAgo())
    }

    @SuppressLint("SimpleDateFormat")
    fun getDaysAgo(): Date {
        val date = Date()
        val calendar = Calendar.getInstance()
        val formatter = SimpleDateFormat("yyyy-MM-dd")
        val formatted: String = formatter.format(date)
        val myDate = formatter.parse(formatted)
        calendar.time = myDate
        calendar.add(Calendar.DAY_OF_YEAR, -10)

        return calendar.time
    }

    private fun getSportNews(from: String, to: String, sortBy: String) {
        val call: Call<MainData> = ApiClient.getClient.getSportNews(from, to, sortBy)
        call.enqueue(object : Callback<MainData> {
            @SuppressLint("ShowToast")
            override fun onFailure(call: Call<MainData>?, t: Throwable?) {
                Toast.makeText(
                    context,
                    getString(R.string.check_internet_connection),
                    Toast.LENGTH_SHORT
                )
            }

            override fun onResponse(call: Call<MainData>?, response: Response<MainData>?) =
                if (response!!.isSuccessful) {
                    articleList = response.body().articles
                    adapter = SportNewsAdapter(articleList, context!!)
                    recyclerViewSportNews.adapter = adapter
                    loadingAnimationLL.visibility = View.GONE
                } else {
                    Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show()
                }
        }
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onClick(v: View?) {
        when (v?.id) {
            selectDateLL.id -> {
                if (dateExpandable.isExpanded)
                    dateExpandable.collapse()
                else
                    dateExpandable.expand()
            }
            selectFromDateBtn.id -> {
                calendar = Calendar.getInstance()
                year = calendar!!.get(Calendar.YEAR)
                month = calendar!!.get(Calendar.MONTH)
                dayOfMonth = calendar!!.get(Calendar.DAY_OF_MONTH)
                datePickerDialog = DatePickerDialog(
                    context!!,
                    DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                        selectFromDateBtn.text = "$year-$month-$day"
                        fromDate = "$year-${month + 1}-$day"
                        if (topClicked) {
                            getSportNews(fromDate, toDate, "popularity")
                        } else {
                            getSportNews(fromDate, toDate, "publishedAt")
                        }
                    }, year, month, dayOfMonth
                )
                datePickerDialog!!.show()
//Belirliği bir aralığın dışında tarih seçildiğinde api kaynaklı hata alınıyor, genelde 1 ay öncesi bile çekilemedi.

            }
            selectToDateBtn.id -> {
                calendar = Calendar.getInstance()
                year = calendar!!.get(Calendar.YEAR)
                month = calendar!!.get(Calendar.MONTH)
                dayOfMonth = calendar!!.get(Calendar.DAY_OF_MONTH)
                datePickerDialog = DatePickerDialog(
                    context!!,
                    DatePickerDialog.OnDateSetListener { datePicker, year, month, day ->
                        selectToDateBtn.text = "$year-$month-$day"
                        toDate = "$year-${month + 1}-$day"
                        if (topClicked) {
                            getSportNews(fromDate, toDate, "popularity")
                        } else {
                            getSportNews(fromDate, toDate, "publishedAt")
                        }
                    }, year, month, dayOfMonth
                )
                datePickerDialog!!.show()


            }
            sortByPopularityBtn.id -> {
                topSelectedSeparator.visibility = View.VISIBLE
                latestSelectedSeparator.visibility = View.GONE

                topClicked = true
                getSportNews(fromDate, toDate, "popularity")

            }
            sortByPublishedAtBtn.id -> {
                topSelectedSeparator.visibility = View.GONE
                latestSelectedSeparator.visibility = View.VISIBLE

                topClicked = false
                getSportNews(fromDate, toDate, "publishedAt")
            }
        }
    }
}