package com.example.ayca.newsapp

import android.content.pm.ActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.ayca.newsapp.model.MainData
import com.example.ayca.newsapp.retrofit.ApiClient
import kotlinx.android.synthetic.main.custom_dialog_design.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }

    override fun onBackPressed() {
        customDialog()
    }

    private fun customDialog() {
        val mDialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_design, null)
        val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
        val mAlertDialog = mBuilder.show()
        mAlertDialog.setCancelable(false)

        mDialogView.ok_btn.setOnClickListener{
            finish()
        }
        mDialogView.cancel_btn.setOnClickListener {
            mAlertDialog.dismiss()
        }
    }

}
