package com.example.ayca.newsapp.retrofit

import com.example.ayca.newsapp.model.MainData
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiInterface {
    @GET("everything?q=football&apiKey=ae68088e70d04639b4950bdc9d546924")
    fun getSportNews(
        @Query("from") from: String,
        @Query("to") to: String,
        @Query("sortBy") sortBy: String
    ): Call<MainData>
}